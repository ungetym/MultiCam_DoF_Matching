#include "data.h"

Data::Data(QObject *parent) : QObject(parent){

}

void Data::checkUnlock(){
    unlock_counter_++;

    if((!success_cam || !success_kinect) && (unlock_counter_ == num_analyzer_checks_-1)){
        unlock_counter_++;
    }
    else if(success_cam && success_kinect && (unlock_counter_ == num_analyzer_checks_-1)){
        DoF.push_back(Score(pattern_depth,pattern_sharpness));
        emit DoFvalueSaved();
        return;
    }

    if(unlock_counter_ == num_analyzer_checks_){
        unlock_counter_ = 0;
        success_cam = true;
        success_kinect = true;
        mutex_analyzer_cam.unlock();
        mutex_analyzer_kinect.unlock();
    }
}

void Data::failUnlock(){

    QObject* sender = this->sender();
    if(sender->objectName() == "Detector_Kinect"){
        success_kinect = false;
        unlock_counter_ += 2;
    }
    else if(sender->objectName() == "Detector_Cam"){
        success_cam = false;
        unlock_counter_ += 2;
    }
    else if(sender->objectName() == "Calculator_Kinect"){
        success_kinect = false;
        unlock_counter_ ++;
    }
    else if(sender->objectName() == "Calculator_Cam"){
        success_cam = false;
        unlock_counter_ ++;
    }

    if(unlock_counter_ == num_analyzer_checks_-1){
        unlock_counter_++;
    }

    if(unlock_counter_ == num_analyzer_checks_){
        unlock_counter_ = 0;
        success_cam = true;
        success_kinect = true;
        mutex_analyzer_cam.unlock();
        mutex_analyzer_kinect.unlock();
    }
}
