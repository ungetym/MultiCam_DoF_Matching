#include "main_window.h"
#include "ui_main_window.h"
#include "logger.h"

Main_Window::Main_Window(QWidget *parent) :
    QMainWindow(parent),
    ui_(new Ui::Main_Window),
    data_organizer_(&data_),
    kinect_(&data_),
    cam_(&data_),
    detector_kinect_(&data_,TYPE_KINECT),
    calc_kinect_(&data_,TYPE_KINECT),
    detector_cam_(&data_,TYPE_USB_CAM),
    calc_cam_(&data_,TYPE_USB_CAM)
{
    ui_->setupUi(this);
    data_.moveToThread(&thread_data_);
    data_organizer_.moveToThread(&thread_data_);
    kinect_.moveToThread(&thread_kinect_);
    cam_.moveToThread(&thread_cam_);
    detector_kinect_.moveToThread(&thread_pattern_kinect_);
    detector_cam_.moveToThread(&thread_pattern_cam_);
    calc_kinect_.moveToThread(&thread_pattern_kinect_);
    calc_cam_.moveToThread(&thread_pattern_cam_);

    qRegisterMetaType< cv::Mat >("cv::Mat&");

    //connect ui to kinect
    connect(ui_->graphics_view_kinect, &Qt_Tools::Graphics_View::imageSet, &data_, &Data::checkUnlock);
    connect(ui_->button_start_kinect, &QPushButton::clicked, &kinect_, &Devices::Kinect::capture);
    connect(ui_->button_stop_kinect, &QPushButton::clicked, [this](){data_.capture_kinect = false;});
    connect(ui_->button_stop_kinect, &QPushButton::clicked, &kinect_, &Devices::Kinect::stop);
    connect(ui_->selector_kinect, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), &kinect_, &Devices::Kinect::init);

    //connect ui to usb cam
    connect(ui_->graphics_view_cam, &Qt_Tools::Graphics_View::imageSet, &data_, &Data::checkUnlock);
    connect(ui_->button_start_cam, &QPushButton::clicked, &cam_, &Devices::USB_Cam::capture);
    connect(ui_->button_stop_cam, &QPushButton::clicked, [this](){data_.capture_usb_cam = false;});
    connect(ui_->selector_cam, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), &cam_, &Devices::USB_Cam::init);

    //if a new image arrives, show it and give it to pattern detectors
    connect(&kinect_, &Devices::Kinect::newImage, ui_->graphics_view_kinect, &Qt_Tools::Graphics_View::setCvImage);
    connect(&kinect_, &Devices::Kinect::newImage, &detector_kinect_, &Pattern_Detector::detectPattern);

    connect(&cam_, &Devices::USB_Cam::newImage, ui_->graphics_view_cam, &Qt_Tools::Graphics_View::setCvImage);
    connect(&cam_, &Devices::USB_Cam::newImage, &detector_cam_, &Pattern_Detector::detectPattern);

    //if pattern is not found, tell data to unlock mutex, otherwise show detected pattern and use it for further calculations
    connect(&detector_kinect_, &Pattern_Detector::patternNotFound, &data_, &Data::failUnlock);
    connect(&detector_kinect_, &Pattern_Detector::detectedPattern, ui_->graphics_view_kinect, &Qt_Tools::Graphics_View::setCvImage);
    connect(&detector_kinect_, &Pattern_Detector::patternDetected, &calc_kinect_, &Pattern_Calculations::getPatternDepth);

    connect(&detector_cam_, &Pattern_Detector::patternNotFound, &data_, &Data::failUnlock);
    connect(&detector_cam_, &Pattern_Detector::detectedPattern, ui_->graphics_view_cam, &Qt_Tools::Graphics_View::setCvImage);
    connect(&detector_cam_, &Pattern_Detector::patternDetected, &calc_cam_, &Pattern_Calculations::calculateSharpness);

    //if calculations were unsuccessful tell data to unlock mutex, otherwise data saves the calculated values and shows the DoF graph
    connect(&calc_kinect_, &Pattern_Calculations::patternDepthCalculated, &data_, &Data::checkUnlock);
    connect(&calc_kinect_, &Pattern_Calculations::patternDepthUnavailable, &data_, &Data::failUnlock);

    connect(&data_, &Data::DoFvalueSaved, &data_organizer_, &Data_Calculations::reorganizeData);
    //connect(&data_organizer_, &Data_Calculations::dataReadyToDisplay, )

    //temporary debug stuff
    connect(&calc_kinect_, &Pattern_Calculations::debugImage, ui_->graphics_view_cam, &Qt_Tools::Graphics_View::setCvImage);

    //connect loggers
    connect(this, &Main_Window::log, ui_->logger, &Qt_Tools::Logger::log);
    connect(&kinect_, &Devices::Kinect::log, ui_->logger, &Qt_Tools::Logger::log);
    connect(&cam_, &Devices::USB_Cam::log, ui_->logger, &Qt_Tools::Logger::log);
    connect(&detector_kinect_, &Pattern_Detector::log, ui_->logger, &Qt_Tools::Logger::log);
    connect(&detector_cam_, &Pattern_Detector::log, ui_->logger, &Qt_Tools::Logger::log);
    connect(&calc_kinect_, &Pattern_Calculations::log, ui_->logger, &Qt_Tools::Logger::log);
    connect(&calc_cam_, &Pattern_Calculations::log, ui_->logger, &Qt_Tools::Logger::log);


    //start threads
    thread_data_.start();
    thread_kinect_.start();
    thread_cam_.start();
    thread_pattern_kinect_.start();
    thread_pattern_cam_.start();
}

Main_Window::~Main_Window()
{
    //close cameras and exit threads
    kinect_.close();
    thread_kinect_.exit();
    cam_.close();
    thread_cam_.exit();

    thread_pattern_kinect_.exit();
    thread_pattern_cam_.exit();
    thread_data_.exit();

    delete ui_;
}

void Main_Window::findCams(){
    STATUS("Searching for cameras...");

    this->repaint();

    int num_kinects = kinect_.findCams();
    int num_cams = cam_.findCams();

    STATUS("Found "+QString::number(num_kinects)+" Kinect camera(s) and "+QString::number(num_cams)+" USB camera(s).");

    ui_->selector_kinect->setEnabled(false);
    ui_->selector_cam->setEnabled(false);

    for(int i = 0; i < num_kinects; i++){
        ui_->selector_kinect->addItem(QString::number(i));
    }
    for(int i = 0; i < num_cams; i++){
        ui_->selector_cam->addItem(QString::number(i));
    }

    ui_->selector_kinect->setEnabled(true);
    ui_->selector_cam->setEnabled(true);

    ui_->selector_kinect->setCurrentIndex(std::min(0,ui_->selector_kinect->count()));
    ui_->selector_cam->setCurrentIndex(std::min(0,ui_->selector_cam->count()));

}
