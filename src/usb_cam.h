#ifndef USB_CAM_H
#define USB_CAM_H
#pragma once

#include "data.h"

#include <ueye.h>

namespace Devices{

//default ueye parameters
const double    DEFAULT_UEYE_EXPOSURE       = 25.0;
const double    DEFAULT_UEYE_GAIN_MASTER    = 0.0;
const double    DEFAULT_UEYE_GAIN_RED       = 18.0;
const double    DEFAULT_UEYE_GAIN_GREEN     = 0.0;
const double    DEFAULT_UEYE_GAIN_BLUE      = 12.0;
const double    DEFAULT_UEYE_GAMMA          = 160.0;
const bool      DEFAULT_UEYE_USE_HW_GAMMA   = false;
const bool      DEFAULT_UEYE_USE_GAIN_BOOST = false;

struct Cam_Params{
  int index;
  HIDS handle;

  int dev_Id;
  long serial;
  char label[16];

  int width;
  int height;
  int channels;
  int image_Id;
  char *image;

  UINT pixel_clock = 86;
  double fps = 60.0;
};

class USB_Cam : public QObject
{
    Q_OBJECT

public:
    USB_Cam(Data* data);

public slots:
    ///
    /// \brief findCams searches for (non Kinect) usb camera devices
    /// \return     number of cams
    ///
    int findCams();

    ///
    /// \brief init starts the camera with specified index
    /// \param idx      index of usb cam to start
    /// \return         true if successful, false if init fails
    ///
    bool init(const int idx = 0);

    ///
    /// \brief capture starts the capturing loop
    ///
    void capture();

    ///
    /// \brief close the usb cam
    ///
    void close();

private:
    Data* data_ = nullptr;
    int active_cam_idx_ = -1;
    std::vector<Cam_Params> cam_params_;

signals:
    void log(QString msg, int type);
    void newImage(cv::Mat& image);
};
}
#endif // USB_CAM_H
