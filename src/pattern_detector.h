#ifndef PATTERN_DETECTOR_H
#define PATTERN_DETECTOR_H
#pragma once

#include <data.h>

#include <QObject>

class Pattern_Detector : public QObject
{
    Q_OBJECT

public:
    ///
    /// \brief Pattern_Detector
    /// \param data
    ///
    explicit Pattern_Detector(Data* data, const int type);

public slots:

    ///
    /// \brief detectPattern
    ///
    void detectPattern();

private:

    Data* data_;
    int type_;

    ///
    /// \brief trackSiemensChecker
    /// \param image
    /// \param center
    /// \param radius
    /// \param corners
    /// \param scale
    /// \return
    ///
    bool trackSiemensChecker(const cv::Mat& image, cv::Point* center, float* radius, std::vector<cv::Point>* corners, const float& scale = 1.0, std::vector<int>* dilations = nullptr);

    ///
    /// \brief detectSiemensChecker
    /// \param image
    /// \param center
    /// \param radius
    /// \param corners
    /// \param scale
    /// \param dilation
    /// \return
    ///
    bool detectSiemensChecker(const cv::Mat& image, cv::Point* center, float* radius, std::vector<cv::Point>* corners, const float& scale = 1.0, const int& dilation = 0.0);

    ///
    /// \brief checkAndRefineCheckerCorner checks if a point p represents a checkerboard corner and refines it by calculating symmetries in a neighborhood around p
    /// \param p            checkerboard corner candidate
    /// \param refined_p    returns the refined corner if p is a checkerboard corner
    /// \param image        current camera image
    /// \param size         the neighborhood of a corner p is a (2size+1)x(2size+1) window around p
    /// \return             true if p is a checkerboard corner
    ///
    bool checkAndRefineCheckerCorner(const cv::Point2f& p, cv::Point2f* refined_p, const cv::Mat &image, const int size = 10, const int refine_area = 3);

    bool checkAndRefineCheckerCorner(const cv::Point& p, cv::Point* refined_p, const cv::Mat &image, const int size = 10, const int refine_area = 3);

    ///
    /// \brief calculateCenter between lines p_1p_4 and p_2p_3
    /// \param p_1
    /// \param p_2
    /// \param p_3
    /// \param p_4
    /// \return
    ///
    cv::Point2f calculateCenter(const cv::Point& p_1, const cv::Point& p_2, const cv::Point& p_3, const cv::Point& p_4);

    ///
    /// \brief findCenters
    /// \param patterns
    /// \param image
    /// \param centers
    /// \param radii
    /// \param pattern_corners
    /// \param check_centers
    /// \return
    ///
    bool findCenters(const std::vector<std::vector<cv::Point>>& patterns, const cv::Mat &image, std::vector<cv::Point2f>* centers, std::vector<float>* radii, std::vector<std::vector<cv::Point>>* pattern_corners, const bool& check_centers = true);

signals:

    void detectedPattern(cv::Mat& image);
    void patternDetected();
    void patternNotFound();
    void sharpnessCalculated();
    void sharpnessNotAvailable();

    void log(QString msg, int type);
};

#endif // PATTERN_DETECTOR_H
