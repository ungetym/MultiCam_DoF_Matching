#include "graphics_view.h"

using namespace cv;

Qt_Tools::Graphics_View::Graphics_View(QWidget *parent){
    this->setParent(parent);
    viewport()->setMouseTracking(true); //activate mouse tracking
    timer_.setInterval(25); //40 hz update rate for mouse movements
    timer_.setSingleShot(true);

    scene_.addItem(&item_);
    setScene(&scene_);
}

void Qt_Tools::Graphics_View::setCvImage(const cv::Mat& image){
    QImage::Format format;
    if(image.type() == CV_8UC1){
        format = QImage::Format_Grayscale8;
    }
    else if(image.type() == CV_8UC3){
        format = QImage::Format_RGB888;
    }
    else if(image.type() == CV_8UC4){
        format = QImage::Format_RGBA8888;
    }
    else if(image.type() == CV_32F){//usually a depth image
        format = QImage::Format_RGB888;
        Mat colored_image;

        image.convertTo(colored_image,CV_8UC1, 255.0/6000.0, 0);
        applyColorMap(colored_image,colored_image,COLORMAP_JET);
        QImage qImage(colored_image.data, colored_image.cols, colored_image.rows, colored_image.step, format);
        setImage(QPixmap::fromImage(qImage));
        emit imageSet();
        return;
    }
    else{
        return;
    }

    QImage qImage(image.data, image.cols, image.rows, image.step, format);
    setImage(QPixmap::fromImage(qImage));

    emit imageSet();
}

void Qt_Tools::Graphics_View::setImage(const QPixmap& image){
    item_.setPixmap(image);
}

void Qt_Tools::Graphics_View::mousePressEvent(QMouseEvent* event){
    if(event->button() == Qt::LeftButton){
        //set initial positions of cursor and scrollbars
        last_cursor_position_ = event->globalPos();
        last_h_bar_pos_ = this->horizontalScrollBar()->value();
        last_v_bar_pos_ = this->verticalScrollBar()->value();
        mouse_pressed_ = true;
    }
}

void Qt_Tools::Graphics_View::mouseReleaseEvent(QMouseEvent* event){
    if(event->button() == Qt::LeftButton){
        mouse_pressed_ = false;
    }
}

void Qt_Tools::Graphics_View::mouseMoveEvent(QMouseEvent* event){
    if(timer_.isActive()){//return in order to prevent signal flooding
        return;
    }

    if(mouse_pressed_){
        //translate scene by modifying scrollbars
        this->horizontalScrollBar()->setValue(last_h_bar_pos_ - (event->globalX()-last_cursor_position_.x()));
        this->verticalScrollBar()->setValue(last_v_bar_pos_ - (event->globalY()-last_cursor_position_.y()));

        //update current positions - needed in order to prevent the mouse from entering a dead (no translation) area
        last_cursor_position_ = event->globalPos();
        last_h_bar_pos_ = this->horizontalScrollBar()->value();
        last_v_bar_pos_ = this->verticalScrollBar()->value();
    }
    timer_.start();
}

void Qt_Tools::Graphics_View::wheelEvent(QWheelEvent* event){
    //rescale the scene
    float old_zoom = zoom_;
    zoom_ += 0.001f*(float)event->delta();
    this->scale(zoom_/old_zoom,zoom_/old_zoom);
    zoom_ = old_zoom;
}
