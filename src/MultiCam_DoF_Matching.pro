QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MultiCam_DoF_Matching
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
        main.cpp \
    main_window.cpp \
    graphics_view.cpp \
    kinect.cpp \
    usb_cam.cpp \
    logger.cpp \
    data.cpp \
    pattern_detector.cpp \
    pattern_calculations.cpp \
    data_calculations.cpp

HEADERS += \
    main_window.h \
    graphics_view.h \
    kinect.h \
    usb_cam.h \
    logger.h \
    data.h \
    pattern_detector.h \
    pattern_calculations.h \
    data_calculations.h


FORMS += \
    main_window.ui


####  include OpenCV  ####

INCLUDEPATH+=/home/tmichels/local_home/opt/opencv_debug/include

LIBS += -L/home/tmichels/local_home/opt/opencv_debug/lib \
-lopencv_rgbd \
-lopencv_highgui \
-lopencv_imgcodecs \
-lopencv_core \
-lopencv_imgproc \
-lopencv_ccalib \
-lopencv_calib3d \
-lopencv_features2d

####  include libfreenect  ####

INCLUDEPATH+=/home/tmichels/local_home/opt/freenect2/include

LIBS += -L/home/tmichels/local_home/opt/freenect2/lib \
-lfreenect2

####  include uEye (USB3) camera libs  ####

LIBS += -L/usr/lib \
-lueye_api

