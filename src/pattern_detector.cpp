#include "pattern_detector.h"
#include "logger.h"

using namespace std;
using namespace cv;

Pattern_Detector::Pattern_Detector(Data* data, const int type){
    if(type == TYPE_KINECT){
        this->setObjectName("Detector_Kinect");
    }
    else{
        this->setObjectName("Detector_Cam");
    }
    data_ = data;
    type_ = type;
}

void Pattern_Detector::detectPattern(){
    bool success = true;
    if(type_ == TYPE_USB_CAM){
        //try to track pattern if previously detected
        cv::Mat image_gray;
        cv::cvtColor(data_->rgb_image_cam,image_gray,cv::COLOR_BGR2GRAY);

        float scale = data_->scale_cam;
        int dilation_success = 0;

        vector<int> dilations = {data_->dilation_cam,data_->dilation_cam-1,data_->dilation_cam+1};
        if(!trackSiemensChecker(image_gray, &data_->pattern_center_cam, &data_->pattern_radius_cam, &data_->pattern_corners_cam, scale, &dilations)){//first try: old values
            scale = 1.0;
            dilations = {0};
            if(!trackSiemensChecker(image_gray, &data_->pattern_center_cam, &data_->pattern_radius_cam, &data_->pattern_corners_cam, scale, &dilations)){//second try: standard values
                //tracking failed: try to re-detect pattern
                scale = 1.0;
                if(!detectSiemensChecker(image_gray, &data_->pattern_center_cam, &data_->pattern_radius_cam, &data_->pattern_corners_cam, scale)){
                    scale = 0.5;
                    if(!detectSiemensChecker(image_gray, &data_->pattern_center_cam, &data_->pattern_radius_cam, &data_->pattern_corners_cam, scale)){
                        scale = 0.25;
                        success = false;
                        dilations = {-2, -1, 0, 1, 2};
                        for(const int& dilation : dilations){
                            if(detectSiemensChecker(image_gray, &data_->pattern_center_cam, &data_->pattern_radius_cam, &data_->pattern_corners_cam, scale, dilation)){
                                dilation_success = dilation;
                                success = true;
                                break;
                            }
                        }
                        if(!success){
                            WARN("Pattern could neither be tracked nor be detected.");
                        }
                    }
                }
            }
        }

        if(success){
            data_->scale_cam = scale;
            if(dilations.size() == 1){
                data_->dilation_cam = dilations[0];
            }
            else{
                data_->dilation_cam = dilation_success;
            }
            //draw pattern position
            cv::circle(data_->rgb_image_cam,data_->pattern_center_cam,data_->pattern_radius_cam,cv::Scalar(255,0,0),4);
            cv::drawMarker(data_->rgb_image_cam,data_->pattern_center_cam,cv::Scalar(255,0,0),MARKER_CROSS,20,4);
            emit detectedPattern(data_->rgb_image_cam);
            emit patternDetected();
        }
        else{
            data_->pattern_radius_cam = 0.0;
            emit patternNotFound();
        }
    }
    if(type_ == TYPE_KINECT){
        //try to track pattern if previously detected
        cv::Mat image_gray;
        cv::cvtColor(data_->rgb_image_kinect,image_gray,cv::COLOR_BGR2GRAY);

        float scale = data_->scale_kinect;
        int dilation_success = 0;

        vector<int> dilations = {data_->dilation_kinect,data_->dilation_kinect-1,data_->dilation_kinect+1};
        if(!trackSiemensChecker(image_gray, &data_->pattern_center_kinect, &data_->pattern_radius_kinect, &data_->pattern_corners_kinect, scale, &dilations)){//first try: old values
            scale = 1.0;
            dilations = {0};
            if(!trackSiemensChecker(image_gray, &data_->pattern_center_kinect, &data_->pattern_radius_kinect, &data_->pattern_corners_kinect, scale, &dilations)){//second try: standard values
                //tracking failed: try to re-detect pattern
                scale = 1.0;
                if(!detectSiemensChecker(image_gray, &data_->pattern_center_kinect, &data_->pattern_radius_kinect, &data_->pattern_corners_kinect, scale)){
                    scale = 0.5;
                    if(!detectSiemensChecker(image_gray, &data_->pattern_center_kinect, &data_->pattern_radius_kinect, &data_->pattern_corners_kinect, scale)){
                        scale = 0.25;
                        success = false;
                        dilations = {-2, -1, 0, 1, 2};
                        for(const int& dilation : dilations){
                            if(detectSiemensChecker(image_gray, &data_->pattern_center_kinect, &data_->pattern_radius_kinect, &data_->pattern_corners_kinect, scale, dilation)){
                                dilation_success = dilation;
                                success = true;
                                break;
                            }
                        }
                        if(!success){
                            WARN("Pattern could neither be tracked nor be detected.");
                        }
                    }
                }
            }
        }

        if(success){
            data_->scale_kinect = scale;
            if(dilations.size() == 1){
                data_->dilation_kinect = dilations[0];
            }
            else{
                data_->dilation_kinect = dilation_success;
            }
            //draw pattern position
            cv::circle(data_->rgb_image_kinect,data_->pattern_center_kinect,data_->pattern_radius_kinect,cv::Scalar(255,0,0),4);
            cv::drawMarker(data_->rgb_image_kinect,data_->pattern_center_kinect,cv::Scalar(255,0,0),MARKER_CROSS,20,4);
            emit detectedPattern(data_->rgb_image_kinect);
            emit patternDetected();
        }
        else{
            data_->pattern_radius_kinect = 0.0;
            emit patternNotFound();
        }
    }
}

bool Pattern_Detector::trackSiemensChecker(const Mat &image, Point* center, float* radius, vector<Point>* corners, const float& scale, vector<int>* dilations){
    //check if pattern was detected in previous image
    if(*radius == 0){
        return false;
    }

    //calculate bounding box around old pattern position + 70% size for pattern movement
    vector<Point> initial_corners;
    int min_x = image.cols;
    int max_x = 0;
    int min_y = image.rows;
    int max_y = 0;
    for(int i = 0; i < 4; i++){
        min_x = min(min_x,corners->at(i).x);
        max_x = max(max_x,corners->at(i).x);
        min_y = min(min_y,corners->at(i).y);
        max_y = max(max_y,corners->at(i).y);
    }
    int x_add = (int)(0.7*(float)(max_x-min_x));
    int y_add = (int)(0.7*(float)(max_y-min_y));
    min_x = max(0,min_x-x_add);
    max_x = min(image.cols-1,max_x+x_add);
    min_y = max(0,min_y-y_add);
    max_y = min(image.rows-1,max_y+y_add);
    Rect bounding_box(min_x,min_y,max_x-min_x,max_y-min_y);

    //scale large image rois down in order to reduce further computations
    cv::Mat image_roi;
    float scaling = min(1.0,200.0/(scale*min(cv::norm(corners->at(0)-corners->at(2)),cv::norm(corners->at(1)-corners->at(3)))));
    scaling *= scale;
    cv::resize(image(bounding_box),image_roi,Size(0,0),scaling,scaling,INTER_CUBIC);

    if(dilations == nullptr){
        dilations = new vector<int>(1,0);
    }

    bool success = false;

    for(int& dilation : *dilations){
        if(dilation != 0){
            int block_size = image.cols/10;
            if( block_size%2 != 1){
                block_size++;
            }
            adaptiveThreshold(image_roi,image_roi,255,ADAPTIVE_THRESH_MEAN_C,CV_THRESH_BINARY,block_size,0);
            if(dilation > 0){
                erode(image_roi, image_roi, Mat(), Point(-1, -1), dilation);
            }
            else{
                dilate(image_roi, image_roi, Mat(), Point(-1, -1), -dilation);
            }
        }

        goodFeaturesToTrack(image_roi,initial_corners,300,0.02,10.0,noArray(),5);

        //in order to detect the calibration pattern at least 4 corners must be detected
        if(initial_corners.size() < 4){
            continue;
        }

        //filter corners according to distance to previous corners
        float area_radius = 0.5*min(cv::norm(corners->at(0)-corners->at(1)),min(cv::norm(corners->at(0)-corners->at(2)),min(cv::norm(corners->at(3)-corners->at(2)),cv::norm(corners->at(3)-corners->at(1)))));
        vector<vector<Point>> filtered_corners(4, vector<Point>());
        Point refined_p;

        for(Point& p : initial_corners){
            //unscale/translate features since these were calculated in a scaled roi of the original image
            Point p_orig = Point((int)((float)p.x/scaling +0.5),(int)((float)p.y/scaling +0.5))+Point(min_x,min_y);

            for(int i = 0; i < 4; i++){
                Point& q = corners->at(i);
                if(cv::norm(p_orig-q) < area_radius){
                    if(checkAndRefineCheckerCorner(p,&refined_p,image_roi)){
                        filtered_corners[i].push_back(Point(min_x+(int)((float)refined_p.x/scaling +0.5),min_y+(int)((float)refined_p.y/scaling +0.5)));
                        break;
                    }
                }
            }
        }

        for(int i = 0; i < 4; i++){
            if(filtered_corners[i].size() == 0){
                continue;
            }
        }

        //search for corners that belong to the same pattern
        vector<vector<Point>> patterns;
        Point p_1,p_2,p_3,p_4;
        float angle, angle_2, ratio, norm_1_2,norm_1_3;

        float ratio_threshold = 0.2;
        float angle_threshold = 8.0;

        for(size_t i = 0; i < filtered_corners[0].size(); i++){
            p_1 = filtered_corners[0][i];
            for(size_t j = 0; j < filtered_corners[1].size(); j++){
                p_2 = filtered_corners[1][j];
                for(size_t k = 0; k < filtered_corners[2].size(); k++){
                    p_3 = filtered_corners[2][k];
                    //calculate length ratio |p3-p1|/|p2-p1|
                    norm_1_2 = norm(p_2-p_1);
                    norm_1_3 = norm(p_3-p_1);
                    ratio = norm_1_2/norm_1_3;
                    if(norm_1_2 < norm_1_3){
                        ratio = 1.0/ratio;
                        //assure, that p1p2 is always the longer edge
                        Point p_temp = p_2;
                        p_2 = p_3;
                        p_3 = p_temp;
                    }
                    //compare length ratio to the ratio 1.782 of the real pattern
                    if( abs(ratio-1.782) < ratio_threshold ){
                        //calculate angle between p3-p1 and p2-p1
                        angle = 180.0/M_PI*abs(acos(((p_3-p_1).x*(p_2-p_1).x+(p_3-p_1).y*(p_2-p_1).y)/(norm(p_3-p_1)*norm(p_2-p_1))));
                        //since the pattern should be mounted parallel to the camera plane, this angle should be approximately pi/2 (or 1.5pi)
                        if(abs(90-angle)<angle_threshold || abs(270-angle)<angle_threshold){
                            for(size_t l = 0; l < filtered_corners[3].size(); l++){
                                p_4 = filtered_corners[3][l];
                                //check if distance between p4 and p1 is sufficient for p4 to be the fourth corner
                                if(norm(p_4-p_1) > max(norm_1_2,norm_1_3)){
                                    //calculate angle between p3-p4 and p2-p4
                                    angle_2 = 180.0/M_PI*abs(acos(((p_3-p_4).x*(p_2-p_4).x+(p_3-p_4).y*(p_2-p_4).y)/(norm(p_3-p_4)*norm(p_2-p_4))));
                                    //since the pattern should be mounted parallel to the camera plane, this angle should also be approximately pi/2 (or 1.5pi)
                                    if(abs(90-angle_2)<angle_threshold || abs(270-angle_2)<angle_threshold){
                                        vector<Point> pattern = {p_1,p_2,p_3,p_4};
                                        patterns.push_back(pattern);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        //add siemens stars based on the detected patterns
        vector<Point2f> centers;
        vector<float> radii;
        vector<vector<Point>> pattern_corners;
        findCenters(patterns, image, &centers, &radii, &pattern_corners, (scale == 1.0));

        if(centers.size() == 0){
            continue;
        }
        else if(centers.size() > 1){
            WARN("Ambiguous pattern tracking result - returning first.");
        }

        *center = centers[0];
        *radius = radii[0];
        *corners = pattern_corners[0];

        success = true;
        *dilations = {dilation};
        break;
    }

    return success;
}

bool Pattern_Detector::detectSiemensChecker(const Mat& image, Point* center, float* radius, vector<Point>* corners, const float& scale, const int& dilation){
    vector<Point> initial_corners;
    cv::Mat image_scaled;
    if(scale != 1.0){
        cv::resize(image,image_scaled,Size(0,0),scale,scale,INTER_CUBIC);
    }
    else{
        image_scaled = image.clone();
    }

    if(dilation != 0){
        int block_size = image.cols/10;
        if( block_size%2 != 1){
            block_size++;
        }
        adaptiveThreshold(image_scaled,image_scaled,255,ADAPTIVE_THRESH_MEAN_C,CV_THRESH_BINARY,block_size,0);
        if(dilation > 0){
            erode(image_scaled, image_scaled, Mat(), Point(-1, -1), dilation);
        }
        else{
            dilate(image_scaled, image_scaled, Mat(), Point(-1, -1), -dilation);
        }
    }

    goodFeaturesToTrack(image_scaled,initial_corners,300,0.02,10.0,noArray(),5);

    //in order to detect the calibration pattern at least 4 corners must be detected
    if(initial_corners.size() < 4){
        WARN("Could not find 4 or more good features.");
        return false;
    }

    //use symmetry constraints on small neighborhoods to filter the initial corners
    vector<Point> filtered_corners;
    for(const Point& p : initial_corners){
        Point refined_p;
        if(checkAndRefineCheckerCorner(p,&refined_p,image_scaled)){
            filtered_corners.push_back(refined_p);
        }
    }

    //search for corners that belong to the same pattern
    vector<bool> in_pattern(filtered_corners.size(),false);
    vector<vector<Point>> patterns;
    Point p_1,p_2,p_3,p_4;
    float angle, angle_2, ratio, norm_1_2,norm_1_3;
    float ratio_threshold = 0.2;
    float angle_threshold = 8.0;

    for(size_t i = 0; i < filtered_corners.size(); i++){
        if(in_pattern[i]){
            continue;
        }
        p_1 = filtered_corners[i];
        for(size_t j = 0; j < filtered_corners.size() && !in_pattern[i]; j++){
            if(in_pattern[j] || i == j){
                continue;
            }
            p_2 = filtered_corners[j];
            for(size_t k = 0; k < filtered_corners.size() && !in_pattern[i]; k++){
                if(in_pattern[k] || k == j || k == i){
                    continue;
                }
                p_3 = filtered_corners[k];

                //calculate length ratio |p3-p1|/|p2-p1|
                norm_1_2 = norm(p_2-p_1);
                norm_1_3 = norm(p_3-p_1);
                ratio = norm_1_2/norm_1_3;
                if(norm_1_2 < norm_1_3){
                    ratio = 1.0/ratio;
                    //assure, that p1p2 is always the longer edge
                    Point p_temp = p_2;
                    p_2 = p_3;
                    p_3 = p_temp;
                }
                //compare length ratio to the ratio 1.782 of the real pattern
                if( abs(ratio-1.782) < ratio_threshold ){
                    //calculate angle between p3-p1 and p2-p1
                    angle = 180.0/M_PI*abs(acos(((p_3-p_1).x*(p_2-p_1).x+(p_3-p_1).y*(p_2-p_1).y)/(norm(p_3-p_1)*norm(p_2-p_1))));
                    //since the pattern should be parallel to the camera plane, this angle should be approximately pi/2 (or 1.5pi)
                    if(abs(90.0-angle)<angle_threshold || abs(270.0-angle)<angle_threshold){
                        for(size_t l = 0; l < filtered_corners.size() && !in_pattern[i]; l++){
                            if(in_pattern[l] || l==k || l == j || l == i){
                                continue;
                            }
                            p_4 = filtered_corners[l];
                            //check if distance between p4 and p1 is sufficient for p4 to be the fourth corner
                            if(norm(p_4-p_1) > max(norm_1_2,norm_1_3)){
                                //calculate angle between p3-p4 and p2-p4
                                angle_2 = 180.0/M_PI*abs(acos(((p_3-p_4).x*(p_2-p_4).x+(p_3-p_4).y*(p_2-p_4).y)/(norm(p_3-p_4)*norm(p_2-p_4))));
                                //since the pattern should be mounted parallel to the camera plane, this angle should also be approximately pi/2 (or 1.5pi)
                                if(abs(90.0-angle_2)<angle_threshold || abs(270.0-angle_2)<angle_threshold){
                                    vector<Point> pattern = {p_1,p_2,p_3,p_4};
                                    patterns.push_back(pattern);
                                    in_pattern[i]=true;
                                    in_pattern[j]=true;
                                    in_pattern[k]=true;
                                    in_pattern[l]=true;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    //check if pattern center is a checkerboard corner
    vector<Point2f> centers;
    vector<float> radii;
    vector<vector<Point>> pattern_corners;

    findCenters(patterns, image_scaled, &centers, &radii, &pattern_corners, (scale == 1.0));

    //
    if(centers.size() == 0){
        return false;
    }

    if(centers.size() > 1){
        WARN("Detected more than one pattern. Automatically using the first one.");
    }

    //set output values and rescale them to fit the original image
    *center = centers[0]/scale;
    *radius = radii[0]/scale;
    corners->clear();
    for(int i = 0; i < 4; i++){
        Point2f corner(pattern_corners[0][i].x,pattern_corners[0][i].y);
        corner /= scale;
        corners->push_back(Point((int)(corner.x+0.5),(int)(corner.y+0.5)));
    }

    return true;
}

bool Pattern_Detector::checkAndRefineCheckerCorner(const Point& p, Point* refined_p, const Mat &image, const int size, const int refine_area){
    cv::Point2f refined_p_f;
    bool success = checkAndRefineCheckerCorner(cv::Point2f(p.x,p.y),&refined_p_f,image,size,refine_area);
    if(success){
        *refined_p = cv::Point((int)(refined_p_f.x+0.5),(int)(refined_p_f.y+0.5));
    }
    return success;
}

bool Pattern_Detector::checkAndRefineCheckerCorner(const Point2f& p, Point2f* refined_p, const Mat &image, const int size, const int refine_area){
    float scale = 1.0;//max(2.0,12.0/(float)size);
    int size_scaled = (int)scale*size;
    int edge_size = 2*size_scaled+1;//edge size of neighborhood upscaled
    double num_of_px = (double)(edge_size*edge_size);//px in upscaled neighborhood

    //create lookup tables for mirroring
    Mat lut_x = Mat(edge_size,edge_size,CV_8UC1);
    Mat lut_y = Mat(edge_size,edge_size,CV_8UC1);
    for(float row = 0; row < lut_x.rows; row++){
        for(float col = 0; col < lut_x.cols; col++){
            float mirrored_x, mirrored_y;
            if(row<col && row+col<2*size_scaled+2){
                mirrored_y = (float)size_scaled - row;
                mirrored_x = col + (mirrored_y - row) * ((float)size_scaled - col) / ((float)size_scaled - row);
            }
            else if(row>col && row+col>2*size_scaled+1){
                mirrored_y = 3.0*(float)size_scaled + 2.0 - row;
                mirrored_x = col + (mirrored_y - row) * ((float)size_scaled - col) / ((float)size_scaled - row);
            }
            else if(row<col && row+col>=2*size_scaled+1){
                mirrored_x = 3.0*(float)size_scaled + 2.0 - col;
                mirrored_y = row + (mirrored_x - col) * ((float)size_scaled - row) / ((float)size_scaled - col);
            }
            else if(row>col && row+col<=2*size_scaled+1){
                mirrored_x = (float)size_scaled - col;
                mirrored_y = row + (mirrored_x - col) * ((float)size_scaled - row) / ((float)size_scaled - col);
            }
            lut_x.at<uchar>((int)row,(int)col) = (uchar)mirrored_x;
            lut_y.at<uchar>((int)row,(int)col) = (uchar)mirrored_y;
        }
    }


    float best_score_point = 0.0;
    float best_score_ring = 0.0;
    Point best_position = p;

    float score_point_symmetry, score_ring_symmetry;
    Point q;

    Mat neighborhood,flipped,mirrored,average;

    auto isValid = [](float a, float b){return (a>0.75)&(b>0.75);};

    //in order to refine the corner position, the symmetry scores are calculated for all points in a refine_areaxrefine_area neighborhood of an initial corner
    for(int i = -refine_area; i < refine_area+1; i++){
        for(int j = -refine_area; j < refine_area+1; j++){
            q = p + Point2f(i,j);

            //calculate bounding box
            Rect bounding_box(q.x-size,q.y-size,2*size+1,2*size+1);
            if(bounding_box.x < 0 || bounding_box.y < 0 || q.x+2*size+2 > image.cols || q.y+2*size+2 > image.rows){//neighborhood not within image bounds
                continue;
            }

            //binarize neighborhood using the average gray value
            neighborhood = image(bounding_box).clone();
            //cv::resize(neighborhood,neighborhood,Size(0.0,0.0),(int)scale,(int)scale,INTER_NEAREST);
            double avg = (double)(sum(neighborhood)[0])/num_of_px;
            threshold(neighborhood,neighborhood,avg,255,CV_THRESH_BINARY);

            //if the thresholded image does not contain 30-70% black pixels, it cannot represent a checkerboard corner
            double num_of_black_px = (double)(sum(neighborhood)[0])/255.0;
            if(num_of_black_px < 0.3*num_of_px || num_of_black_px > 0.7*num_of_px){
                continue;
            }

            //create mirrored neighborhoods via flip and mirroring at rectangle via lut
            flip(neighborhood,flipped,-1);

            mirrored = Mat(neighborhood.rows,neighborhood.cols,neighborhood.type());
            for(int row = 0; row < mirrored.rows; row++){
                for(int col = 0; col < mirrored.cols; col++){
                    mirrored.at<uchar>(row,col) = neighborhood.at<uchar>(lut_y.at<uchar>(row,col),lut_x.at<uchar>(row,col));
                }
            }

            //compare mirrored/flipped to original neighborhood in order to determine the symmetry
            average = 0.5*(flipped+neighborhood);
            float correct_px = 0;
            float wrong_px = 0;
            for(int px = 0; px<average.rows*average.cols; px++){
                if(average.data[px] == 0 || average.data[px] == 255){
                    correct_px++;
                }
                else{
                    wrong_px++;
                }
            }
            average = 0.5*(mirrored+neighborhood);
            float correct_px_mirror = 0;
            float wrong_px_mirror = 0;
            for(int px = 0; px<average.rows*average.cols; px++){
                if(average.data[px] == 0 || average.data[px] == 255){
                    correct_px_mirror++;
                }
                else{
                    wrong_px_mirror++;
                }
            }
            //calculate scores
            score_point_symmetry = correct_px/(correct_px+wrong_px);
            score_ring_symmetry = correct_px_mirror/(correct_px_mirror+wrong_px_mirror);
            //            if(size != 10){
            //                std::cerr << score_point_symmetry << " / " << score_ring_symmetry << "\n";
            //            }
            if(isValid(score_point_symmetry,score_ring_symmetry) & !isValid(best_score_point,best_score_ring)){
                best_score_point = score_point_symmetry;
                best_score_ring = score_ring_symmetry;
                best_position = q;
            }
            else if(score_point_symmetry+score_ring_symmetry > best_score_point+best_score_ring){
                best_score_point = score_point_symmetry;
                best_score_ring = score_ring_symmetry;
                best_position = q;
            }
        }
    }


    //high symmetry scores indicate checkerboard corners
    if(isValid(best_score_point,best_score_ring)){
        //        if(size != 10){
        //            std::cerr << "Ok "<< best_score_point << " / " << best_score_ring << "\n";
        //        }
        *refined_p = best_position;
        return true;
    }
    //    else{
    //        if(size != 10){
    //            std::cerr << "Fail " << best_score_point << " / " << best_score_ring << "\n";
    //        }
    //    }

    return false;
}

Point2f Pattern_Detector::calculateCenter(const Point& p_1, const Point& p_2, const Point& p_3, const Point& p_4){
    int p_x = (p_1.x*p_4.y-p_1.y*p_4.x)*(p_2.x-p_3.x)-(p_1.x-p_4.x)*(p_2.x*p_3.y-p_2.y*p_3.x);
    int p_y = (p_1.x*p_4.y-p_1.y*p_4.x)*(p_2.y-p_3.y)-(p_1.y-p_4.y)*(p_2.x*p_3.y-p_2.y*p_3.x);
    int p_z = (p_1.x-p_4.x)*(p_2.y-p_3.y)-(p_1.y-p_4.y)*(p_2.x-p_3.x);
    p_x = (float)p_x/(float)p_z;
    p_y = (float)p_y/(float)p_z;
    return Point2f(p_x,p_y);
}

bool Pattern_Detector::findCenters(const vector<vector<Point>>& patterns, const Mat& image, vector<Point2f>* centers, vector<float>* radii, vector<vector<Point>>* pattern_corners, const bool& check_centers){
    bool success = false;

    if(centers == nullptr || radii == nullptr || pattern_corners == nullptr){
        WARN("Centers could not be checked.");
        return false;
    }

    centers->clear();
    radii->clear();
    pattern_corners->clear();

    for(const vector<Point>& pat : patterns){
        //calculate midpoint of siemens star
        Point2f center_proposal = calculateCenter(pat[0],pat[1],pat[2],pat[3]);
        float distance = 0.05*min(cv::norm(pat[0]-pat[2]),cv::norm(pat[1]-pat[3]));
        bool center_valid = true;
        if(check_centers && distance > 4.0){
            center_valid = checkAndRefineCheckerCorner(center_proposal,&center_proposal,image,std::max(2,(int)(0.5*distance +0.5)),std::max(5,(int)(0.5*distance +0.5)));
        }
        if(center_valid){
            //calculate radius based on the known geometry of the pattern
            float diagonal_1 = norm(pat[0]-pat[3]);//diagonal between p0 and p3
            float diagonal_2 = norm(pat[1]-pat[2]);//diagonal between p1 and p2
            float radius_proposal = 0.5896*0.25*(diagonal_1+diagonal_2);//the circle radius is approximately 0.5896*0.5*diagonal_of_pattern

            centers->push_back(center_proposal);
            radii->push_back(radius_proposal);
            pattern_corners->push_back(pat);

            success = true;
        }
    }

    return success;
}
