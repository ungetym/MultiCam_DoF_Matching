#ifndef PATTERN_CALCULATIONS_H
#define PATTERN_CALCULATIONS_H
#pragma once

#include "data.h"
#include "logger.h"

#include <QObject>

class Pattern_Calculations : public QObject{
    Q_OBJECT

public:

    ///
    /// \brief Pattern_Calculations standard constructor
    /// \param data
    ///
    explicit Pattern_Calculations(Data* data, const int type);


public slots:

    ///
    /// \brief getPatternDepth uses the Kinect intrinsics and the calculated pattern position in the Kinect rgb image to calculate the pattern depth
    ///
    void getPatternDepth();

    ///
    /// \brief calculateSharpness uses the detected siemens star from the usb cam rgb image to calculate a sharpness value based on the contrasts
    ///
    void calculateSharpness();


private:

    Data* data_;

    /// Helper ///
    float distPointLine(const  cv::Point& l_1, const  cv::Point& l_2, const  cv::Point& p);
    float distPointLine(const  cv::Point2f& l_1, const  cv::Point2f& l_2, const  cv::Point2f& p);
    cv::Point2f calculateCenter(const cv::Point& p_1, const cv::Point& p_2, const cv::Point& p_3, const cv::Point& p_4);


signals:

    void log(QString msg, int type);

    void patternDepthCalculated();
    void patternDepthUnavailable();

    //for debug
    void debugImage(cv::Mat& image);
};

#endif // PATTERN_CALCULATIONS_H
