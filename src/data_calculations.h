#ifndef DATA_CALCULATIONS_H
#define DATA_CALCULATIONS_H
#pragma once

#include "data.h"
#include "logger.h"

#include <QObject>

class Data_Calculations : public QObject
{
    Q_OBJECT
public:
    explicit Data_Calculations(Data* data);


public slots:

    void reorganizeData();

private:

    Data* data_;

signals:

    void dataReadyToDisplay();

};

#endif // DATA_CALCULATIONS_H
