#include "usb_cam.h"
#include "logger.h"

#include <QThread>

using namespace std;

Devices::USB_Cam::USB_Cam(Data* data){
    this->setObjectName("USB_Cam");
    data_ = data;
}

int Devices::USB_Cam::findCams(){
    // Get cameras
    int num_cams = 0;
    //get number of cameras
    if (is_GetNumberOfCameras(&num_cams) == IS_SUCCESS && num_cams > 0) {
        //get list of cameras
        UEYE_CAMERA_LIST *list = (UEYE_CAMERA_LIST*) malloc(sizeof(DWORD) + num_cams * sizeof(UEYE_CAMERA_INFO));
        list->dwCount = num_cams;
        if (is_GetCameraList(list) == IS_SUCCESS) {
            num_cams = list->dwCount;
            if (num_cams > 0) {
                //save camera parameters
                for (int i = 0; i < num_cams; i++) {
                    Cam_Params new_params;
                    new_params.index = i;
                    new_params.dev_Id = (int) list->uci[i].dwDeviceID;
                    new_params.serial = atol(list->uci[i].SerNo);
                    memset(new_params.label, 0, 16);
                    sprintf(new_params.label, "%ld", new_params.serial % 1000000000000L);
                    new_params.handle = 0;
                    new_params.width = 0;
                    new_params.height = 0;
                    new_params.channels = 0;
                    new_params.image = nullptr;
                    cam_params_.push_back(new_params);
                }
            }
            else{
                ERROR("No uEye cameras found.");
                return 0;
            }
        }
        free(list);
    }
    else{
        ERROR("No uEye cameras found.");
        return 0;
    }

    return num_cams;
}

bool Devices::USB_Cam::init(const int idx){
    STATUS("Initialize USB camera..");

    // Connect camera
    if(idx >= (int) cam_params_.size()){
        ERROR("Invalid usb cam index.");
        return false;
    }
    Cam_Params& params = cam_params_[idx];
    HIDS cam = (HIDS) ((INT)params.dev_Id | IS_USE_DEVICE_ID);
    if(is_InitCamera(&cam, NULL) != IS_SUCCESS){
        ERROR("Unable to initialize camera.");
        return false;
    }
    params.handle = cam;

    // Set pixel clock and frame rate
    if (is_PixelClock(cam, IS_PIXELCLOCK_CMD_SET, (void*)&params.pixel_clock, sizeof(params.pixel_clock)) != IS_SUCCESS){
        ERROR("Failed to set pixel clock!");
        return false;
    }
    if (is_PixelClock(cam, IS_PIXELCLOCK_CMD_GET, (void*)&params.pixel_clock, sizeof(params.pixel_clock)) != IS_SUCCESS){
        ERROR("Failed to get pixel clock!");
        params.pixel_clock = 0;
        return false;
    }

    double new_fps;
    if (is_SetFrameRate(cam, params.fps, &new_fps)){
        WARN("Setting framerate failed.");
    }
    params.fps = new_fps;

    // Set parameters
    double dummy = 0.0;
    double auto_gain = 0.0, auto_shutter = 0.0, auto_white_balance = 0.0;
    double exposure = DEFAULT_UEYE_EXPOSURE;
    INT gamma = (INT)DEFAULT_UEYE_GAMMA;
    const bool use_hw_gamma = DEFAULT_UEYE_USE_HW_GAMMA;
    const bool use_gain_boost = DEFAULT_UEYE_USE_GAIN_BOOST;

    if (is_SetHardwareGamma(cam, IS_GET_HW_SUPPORTED_GAMMA) == IS_SET_HW_GAMMA_ON) {
        if (is_SetHardwareGamma(cam, use_hw_gamma ? IS_SET_HW_GAMMA_ON : IS_SET_HW_GAMMA_OFF) != IS_SUCCESS) {
            WARN("Failed to en/disable hardware gamma!");
        }
    }
    if (is_SetAutoParameter(cam, IS_SET_ENABLE_AUTO_GAIN, &auto_gain, &dummy) != IS_SUCCESS) {
        WARN("Failed to set auto gain parameter!");
    }
    if (is_SetAutoParameter(cam, IS_SET_ENABLE_AUTO_SHUTTER, &auto_shutter, &dummy) != IS_SUCCESS) {
        WARN("Failed to set auto shutter parameter!");
    }
    if (is_SetAutoParameter(cam, IS_SET_ENABLE_AUTO_WHITEBALANCE, &auto_white_balance, &dummy) != IS_SUCCESS) {
        WARN("Failed to set auto whitebalance parameter!");
    }
    if (is_Gamma(cam, IS_GAMMA_CMD_SET, &gamma, sizeof(gamma)) != IS_SUCCESS) {
        WARN("Failed to set gamma parameter!");
    }
    if (is_Exposure(cam, IS_EXPOSURE_CMD_SET_EXPOSURE, &exposure, sizeof(exposure)) != IS_SUCCESS) {
        WARN("Failed to set exposure parameter!");
    }
    if (is_SetGainBoost(cam, IS_GET_SUPPORTED_GAINBOOST) == IS_SET_GAINBOOST_ON) {
        if (is_SetGainBoost(cam, use_gain_boost ? IS_SET_GAINBOOST_ON : IS_SET_GAINBOOST_OFF) != IS_SUCCESS) {
            WARN("Failed to en/disable gain boost!");
        }
    }
    if (is_SetHardwareGain(cam, DEFAULT_UEYE_GAIN_MASTER, DEFAULT_UEYE_GAIN_RED,DEFAULT_UEYE_GAIN_GREEN, DEFAULT_UEYE_GAIN_BLUE) != IS_SUCCESS) {
        WARN("Failed to set gain parameter!");
    }

    // Set color mode to raw (bayern pattern image)
    if (is_SetColorMode(cam, IS_CM_SENSOR_RAW8) != IS_SUCCESS) {
        ERROR("Failed to set color mode!");
        return false;
    }

    // Set image format
    int width = 1280, height = 720;
    UINT image_format = 9;
    UINT num_formats;
    UINT format_size = sizeof(IMAGE_FORMAT_LIST);
    is_ImageFormat(cam, IMGFRMT_CMD_GET_NUM_ENTRIES, &num_formats, sizeof(num_formats));
    format_size += (num_formats-1) * sizeof(IMAGE_FORMAT_INFO);
    void *format_buffer = malloc(format_size);
    IMAGE_FORMAT_LIST *format_list = (IMAGE_FORMAT_LIST*) format_buffer;
    format_list->nSizeOfListEntry = sizeof(IMAGE_FORMAT_INFO);
    format_list->nNumListElements = num_formats;
    is_ImageFormat(cam, IMGFRMT_CMD_GET_LIST, format_list, format_size);
    for (UINT i = 0; i < num_formats; i++) {
        IMAGE_FORMAT_INFO format_info = format_list->FormatInfo[i];
        width = format_info.nWidth;
        height = format_info.nHeight;
        image_format = format_info.nFormatID;
        break;
    }
    if (is_ImageFormat(cam, IMGFRMT_CMD_SET_FORMAT, &image_format, sizeof(image_format)) != IS_SUCCESS) {
        ERROR("Failed to set image format!");
        return false;
    }

    // Set image memory and display mode
    char *image = NULL;
    params.image_Id = 0;
    if (is_AllocImageMem(cam, width, height, 8, &image, &params.image_Id) != IS_SUCCESS) {
        ERROR("Failed to allocate image memory!");
        return false;
    }
    if (is_SetImageMem(cam, image, params.image_Id) != IS_SUCCESS) {
        ERROR("Failed to set image memory!");
        return false;
    }
    if (is_SetDisplayMode(cam, IS_SET_DM_DIB) != IS_SUCCESS) {
        ERROR("Failed to set display mode!");
        return false;
    }
    params.width = width;
    params.height = height;
    params.channels = 3;
    params.image = image;

    active_cam_idx_ = idx;

    STATUS("USB camera successfully initialized.");

    return true;
}

void Devices::USB_Cam::capture(){
    if(active_cam_idx_ == -1){
        ERROR("No USB camera initialized.");
        return;
    }

    STATUS("Start capturing USB camera stream..");

    Cam_Params& params = cam_params_[active_cam_idx_];

    // Start running
    data_->capture_usb_cam = true;
    while (data_->capture_usb_cam) {
        if(data_->mutex_analyzer_cam.try_lock()){//wait for analyzer to finish calculations before writing new images
            if(data_->mutex_cam.try_lock()){
                //get new images and save to cv::Mats
                if(is_FreezeVideo(params.handle, IS_WAIT) != IS_SUCCESS) {
                    ERROR("Failed to capture image of camera.");
                    //data_->capture_usb_cam = false;
                    data_->mutex_cam.unlock();
                    data_->mutex_analyzer_cam.unlock();
                }
                else{
                    //COLOR_BayerBG2RGB
                    cv::cvtColor(cv::Mat(params.height, params.width, CV_8UC1, params.image),data_->rgb_image_cam,cv::COLOR_BayerBG2RGB);
                    data_->mutex_cam.unlock();
                    emit newImage(data_->rgb_image_cam);
                }
            }
        }
        else{
            QThread::msleep(25);
        }
    }
}

void Devices::USB_Cam::close(){
    STATUS("Closing USB camera..");
    if(active_cam_idx_ == -1){
        ERROR("No USB camera initialized.");
        return;
    }

    Cam_Params& params = cam_params_[active_cam_idx_];

    // Release image memory
    if (is_FreeImageMem(params.handle, params.image, params.image_Id) != IS_SUCCESS) {
        ERROR("Failed to release image memory!");
    }
    params.image = nullptr;

    // Release camera
    if (is_ExitCamera(params.handle) != IS_SUCCESS){
        ERROR("Failed to release camera!");
    }
    else{
        params.handle = 0;
        STATUS("USB Camera closed.");
    }
}
