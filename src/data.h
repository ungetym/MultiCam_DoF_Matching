#ifndef DATA_H
#define DATA_H
#pragma once

#include <opencv2/opencv.hpp>

#include <QMutex>
#include <QObject>

struct Distortion_Params{
    float k_1;
    float k_2;
    float k_3;
    float p_1;
    float p_2;

    Distortion_Params(){}

    Distortion_Params(float k_1, float k_2, float k_3, float p_1, float p_2){
        this->k_1 = k_1;
        this->k_2 = k_2;
        this->k_3 = k_3;
        this->p_1 = p_1;
        this->p_2 = p_2;
    }
};

struct Score{
    float depth;
    float sharpness;

    Score(){}

    Score(float depth, float sharpness){
        this->depth = depth;
        this->sharpness = sharpness;
    }
};

struct Freq_Contrast{
    float radius;
    float contrast;

    Freq_Contrast(){}

    Freq_Contrast(float radius, float contrast){
        this->radius = radius;
        this->contrast = contrast;
    }
};

const int TYPE_KINECT = 0;
const int TYPE_USB_CAM = 1;

///
/// \brief The Data class contains the data that is shared between different threads
///
class Data : public QObject
{
    Q_OBJECT

public:
    explicit Data(QObject *parent = nullptr);

    /////////// kinect data ///////////

    bool capture_kinect = false;
    QMutex mutex_analyzer_kinect;
    QMutex mutex_kinect;
    u_int32_t timestamp_kinect;
    cv::Mat rgb_image_kinect;
    cv::Mat ir_image_kinect;
    cv::Mat depth_image_kinect;

    //detected pattern from Kinect rgb image
    cv::Point pattern_center_kinect;
    float pattern_radius_kinect = 0.0;
    std::vector<cv::Point> pattern_corners_kinect;
    //image modification parameters of last successful pattern detection
    float scale_kinect = 1.0;
    int dilation_kinect = 0;

    //intrinsics for the Kinect's IR and RGB camera
    cv::Mat K_ir;
    cv::Mat K_rgb_inv;
    Distortion_Params distortion_ir;

    //detected depth of pattern
    bool success_kinect = true;
    float pattern_depth = 0.0;

    /////////// usb cam data ///////////

    bool capture_usb_cam = false;
    QMutex mutex_analyzer_cam;
    QMutex mutex_cam;
    u_int32_t timestamp_cam;
    cv::Mat rgb_image_cam;

    //detected pattern from usb camera's rgb image
    cv::Point pattern_center_cam;
    float pattern_radius_cam = 0.0;
    std::vector<cv::Point> pattern_corners_cam;
    //image modification parameters of last successful pattern detection
    float scale_cam = 1.0;
    int dilation_cam = 0;

    //detected depth of pattern
    bool success_cam = true;
    std::vector<Freq_Contrast> pattern_sharpness;

    /////////// sharpness data ///////////

    std::vector<std::pair<float, std::vector<Freq_Contrast>>> sharpness;
    std::vector<Score> DoF;

public slots:

    void checkUnlock();
    void failUnlock();

private:

    const int num_analyzer_checks_ = 7;
    int unlock_counter_ = 0;

signals:

    void DoFvalueSaved();

};

#endif // DATA_H
