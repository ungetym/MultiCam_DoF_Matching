#include "pattern_calculations.h"

using namespace cv;
using namespace std;

Pattern_Calculations::Pattern_Calculations(Data *data, const int type){
    if(type == TYPE_KINECT){
        this->setObjectName("Calculations_Kinect");
    }
    else{
        this->setObjectName("Calculations_Cam");
    }
    data_ = data;

}

void Pattern_Calculations::getPatternDepth(){
    //get last pattern depth
    float last_depth = 2000.0;
    if(data_->pattern_depth != 0.0){
        last_depth = data_->pattern_depth;
    }

    //project current detection from rgb image to ir image using the camera intrinsics
    if(data_->pattern_corners_kinect.size() != 4){
        ERROR("Need 4 corners to estimate the pattern depth.");
        return;
    }
    vector<Point> projected;
    for(const Point& p : data_->pattern_corners_kinect){
        vector<float> v_data = {last_depth*(float)p.x,last_depth*(float)p.y,last_depth};
        Mat v(3,1,CV_32F,v_data.data());
        v = data_->K_ir*(data_->K_rgb_inv*v+Mat(Vec3f(50.0,0.0,0.0)));
        if(v.at<float>(2,0) <= 0.0){
            ERROR("Projection failed - z coordinate is non-positive.");
            return;
        }
        projected.push_back(Point((int)(v.at<float>(0,0)/v.at<float>(2,0)+0.5),(int)(v.at<float>(1,0)/v.at<float>(2,0)+0.5)));
    }

    //check if at least one projection point is within image bounds
    vector<bool> in_image;
    for(const Point& p :projected){
        if(p.x < 0 || p.x > data_->depth_image_kinect.cols-1 || p.y < 0 || p.y > data_->depth_image_kinect.rows-1){
            in_image.push_back(false);
        }
        else{
            in_image.push_back(true);
        }
    }
    if(!in_image[0] && !in_image[1] && !in_image[2] && !in_image[3]){
        emit patternDepthUnavailable();
        return;
    }

    //get average depth value of the area defined by the intersection of the depth image and the are between the four cornerpoints
    Point2f center_f = calculateCenter(projected[0],projected[1],projected[2],projected[3]);
    Point center((int)(center_f.x+0.5),(int)(center_f.y+0.5));

    if(center.x < 1 || center.x > data_->depth_image_kinect.cols-2 || center.y < 1 || center.y > data_->depth_image_kinect.rows-2){
        emit patternDepthUnavailable();
        return;
    }
    float radius = norm(projected[0]-projected[1]);

    for(int i = 0; i < 4; i++){
        radius = min(radius, distPointLine(projected[i],projected[(i+1)%4],center));
    }

    radius = min(radius, (float)center.x);
    radius = min(radius, (float)abs(center.x-data_->depth_image_kinect.cols));
    radius = min(radius, (float)center.y);
    radius = min(radius, (float)abs(center.y-data_->depth_image_kinect.rows));

    radius /= sqrt(2.0);
    int size = 2*(int)radius+1;
    cv::Mat inner_area = data_->depth_image_kinect(Rect(center.x-(int)radius,center.y-(int)radius,size,size)).clone();
    int num_non_zero_px = cv::countNonZero(inner_area);
    if(num_non_zero_px == 0){
        emit patternDepthUnavailable();
        return;
    }
    float avg = (float)(sum(inner_area)[0])/(float)(num_non_zero_px);

    data_->pattern_depth = avg;
    emit patternDepthCalculated();

//    /////////////// debug ///////////////
//    cv::Mat depth_img_copy = data_->depth_image_kinect.clone();
//    for(const Point& p :projected){
//        drawMarker(depth_img_copy, p, Scalar(255,0,0),MARKER_CROSS,20,2);
//        circle(depth_img_copy,center,radius,Scalar(255,0,0),2);
//    }
//    emit debugImage(depth_img_copy);
}

void Pattern_Calculations::calculateSharpness(){

}



//////////////////////////////////////////////////////  Helper  //////////////////////////////////////////////////////



float Pattern_Calculations::distPointLine(const Point& l_1, const Point& l_2, const Point& p){
    return distPointLine(Point2f(l_1),Point2f(l_2),Point2f(p));
}

float Pattern_Calculations::distPointLine(const Point2f& l_1, const Point2f& l_2, const Point2f& p){
    Point2f v = l_2-l_1;
    v /= norm(v);
    float s = p.cross(v)-l_1.cross(v);
    Point2f q = l_1 + s*v;
    return norm(q-p);
}

Point2f Pattern_Calculations::calculateCenter(const Point& p_1, const Point& p_2, const Point& p_3, const Point& p_4){
    int p_x = (p_1.x*p_4.y-p_1.y*p_4.x)*(p_2.x-p_3.x)-(p_1.x-p_4.x)*(p_2.x*p_3.y-p_2.y*p_3.x);
    int p_y = (p_1.x*p_4.y-p_1.y*p_4.x)*(p_2.y-p_3.y)-(p_1.y-p_4.y)*(p_2.x*p_3.y-p_2.y*p_3.x);
    int p_z = (p_1.x-p_4.x)*(p_2.y-p_3.y)-(p_1.y-p_4.y)*(p_2.x-p_3.x);
    p_x = (float)p_x/(float)p_z;
    p_y = (float)p_y/(float)p_z;
    return Point2f(p_x,p_y);
}
