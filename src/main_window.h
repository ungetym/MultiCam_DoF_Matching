#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H
#pragma once

#include "data.h"
#include "data_calculations.h"
#include "kinect.h"
#include "pattern_calculations.h"
#include "pattern_detector.h"
#include "usb_cam.h"

#include <QMainWindow>
#include <QThread>

namespace Ui {
class Main_Window;
}

class Main_Window : public QMainWindow
{
    Q_OBJECT

public:
    explicit Main_Window(QWidget *parent = 0);
    ~Main_Window();

    void findCams();

private:
    Ui::Main_Window *ui_;
    QThread thread_data_;
    Data data_;
    Data_Calculations data_organizer_;

    QThread thread_kinect_;
    Devices::Kinect kinect_;

    QThread thread_cam_;
    Devices::USB_Cam cam_;

    QThread thread_pattern_kinect_;
    Pattern_Detector detector_kinect_;
    Pattern_Calculations calc_kinect_;

    QThread thread_pattern_cam_;
    Pattern_Detector detector_cam_;
    Pattern_Calculations calc_cam_;

signals:
    void log(QString msg, int type);

};

#endif // MAIN_WINDOW_H
